# Shakti / InCore Development Environment Image
FROM debian:10

# Moves the OpenOCD Patch into Shakti Development Environment
COPY openocd.patch .

# SET ENVIRONMENT VARIABLES
# This allows the user to actualy invoke all of the commands that we compile in this docker image, and tells the build processes where they can find the riscv toolchain.
ENV PATH=$PATH:/bluespec/bin
ENV RISCV=/riscvtoolchain
ENV PATH=$PATH:/riscvtoolchain/bin
ENV PATH=$PATH:$RISCV/bin:$RISCV/riscv32/bin:$RISCV/riscv64/bin
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$RISCV/riscv32/lib:$RISCV/riscv64/lib
ENV PATH=$PATH:$RISCV/riscv32/riscv32-unknown-elf/bin
ENV PATH=$PATH:$RISCV/riscv32/riscv32-unknown-elf/lib
ENV PATH=$PATH:$RISCV/riscv64/riscv64-unknown-elf/bin
ENV PATH=$PATH:$RISCV/riscv64/riscv64-unknown-elf/lib

# UPDATE / UPGRADE
# This step ensures that the docker image is fully up to date and is ready for dependency installation.
RUN echo "Building CPU simulator build toolchain" ; \
	apt update ; \
	apt -y dist-upgrade

# ALL DEPENDENCIES
# This step installs all of the dependencies needed to build the tooling necessary to build a Shakti or Chromite Core.
RUN apt install -y ghc xxd libghc-regex-compat-dev libghc-syb-dev libghc-split-dev libghc-old-time-dev libfontconfig1-dev libx11-dev libxft-dev flex bison tcl-dev tk-dev libfontconfig1-dev libx11-dev libxft-dev gperf iverilog tcl-itcl4-dev tk-itk4-dev git make libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl g++ autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential texinfo libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev python3 python3-pip libusb-1.0


# INSTALL BLUESPEC (BSC)
# This step installs the bluespec compiler, which takes the code that describes the core in .bsv files and turns it into Verilog and an executable.
RUN git clone --recursive https://github.com/B-Lang-org/bsc ; \
    cd bsc ; \
    mkdir /bluespec ; \
	make PREFIX=/bluespec
    # New multithreaded build comes from Shakti Chromite dev env setup script: https://gitlab.com/incoresemi/utils/common_utils/-/blob/master/chromite/tools_setup.sh.  Hope it works!
	# Unlike chromite dev env setup script, this builds from the master branch of Bluespec.
	# Multithreaded build using this style: https://github.com/B-Lang-org/bsc/blob/393c8ba9537b59b39608afad513063d516d18004/.github/workflows/build.yml#L35
	# Dev env changes frequently, so ensuring that it's well-optomized is a concern
    # Multithreaded build fails when this command is like:  "make -j $(nproc) GHCJOBS=$(nproc) GHCRTSFLAGS='+RTS -M5G -A128m -RTS' PREFIX=/bluespec"
    # LOL.  Back at single threaded because multithreaded fails like: 	make -j $(nproc) PREFIX=/bluespec GHCJOBS=$(nproc)


# MAKE PYTHON 3 DEFAULT AND FIX PIP
# This step ensures that python 3.7 is default. 
# I did not follow the Incore script for this, because Docker is a single-use build environment and there's therefore no need for venv.
RUN update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1 ; \
	update-alternatives --install /usr/bin/python python /usr/bin/python3.7 2 ; \
	ln -s /usr/bin/pip3 /usr/bin/pip

# VERILATOR
RUN cd ~/ ; \
	git clone https://git.veripool.org/git/verilator ; \
	cd verilator ; \
	git checkout stable ; \
	autoconf ; \
	./configure ; \
	make -j $(nproc) ; \
	make install
	
# MOD-SPIKE
# Using directions from this repository: https://gitlab.com/shaktiproject/tools/mod-spike/-/tree/bump-to-latest
RUN git clone https://gitlab.com/shaktiproject/tools/mod-spike.git ; \                                  
    cd mod-spike ; \                                                                                   
    git checkout bump-to-latest ; \      
    git clone https://github.com/riscv/riscv-isa-sim.git ; \
    cd riscv-isa-sim ; \
    git checkout 6d15c93fd75db322981fe58ea1db13035e0f7add ; \
    git apply ../shakti.patch ; \
    mkdir build ; \
    cd build ; \
    ../configure --prefix=$RISCV ; \
    make ; \
    make install

# RISCV-GNU-TOOLCHAIN
RUN cd ~/ ; \
	git clone https://github.com/riscv/riscv-gnu-toolchain ; \
	cd riscv-gnu-toolchain ; \
	git submodule init ; \
	sed -i '/qemu/d' .gitmodules ; \
	sed -i '/qemu/d' .git/config ; \
	git submodule update --recursive ; \
	./configure --prefix=$RISCV ; \
	make -j $(nproc) 

# NOTES FROM CHROMITE BUILD SCRIPT FOR REFERENCE
# clone_repo "https://github.com/riscv/riscv-gnu-toolchain" "9ef0948c4b3a71088cf4df6be93d8e014ba27731"
#  mkdir -p build
#  export RISCV=$PWD/build
#  cd build
#  ../configure --prefix=$RISCV
#  make -j ${JOBS}

# RISC V OPENOCD
# This now installs a patched version of OpenOCD
RUN git clone https://github.com/riscv/riscv-openocd ; \
	cd riscv-openocd ; \
	git checkout 95a8cd9b5d07501ac2243c61f331b092b1ea9894 ; \
	git apply ../openocd.patch ; \
	./bootstrap ; \
	./configure --enable-jlink --enable-remote-bitbang --enable-jtag_vpi --enable-ftdi --prefix=$RISCV ; \
	make -j $(nproc) ; \
	make install

# FORMER RISC V OPENOCD
#RUN cd ~/ ; \
#	git clone https://github.com/riscv/riscv-openocd --recursive -j $(nproc) ; \
#	cd riscv-openocd ; \
#	./bootstrap ; \
#	./configure --enable-jlink --enable-remote-bitbang --enable-jtag_vpi --enable-ftdi --prefix=$RISCV ; \
#	make install

# DEVICE TREE COMPILER
RUN cd ~/ ; \
	wget https://git.kernel.org/pub/scm/utils/dtc/dtc.git/snapshot/dtc-1.4.7.tar.gz ; \
	tar -xvzf dtc-1.4.7.tar.gz ; \
	cd dtc-1.4.7/ ; \
	make NO_PYTHON=1 PREFIX=/usr/ ; \
	make install NO_PYTHON=1 PREFIX=/usr/
