# bscdevenv

Docker image generator for a Chromite / C Class CPU.  

This work is being moved to [another repository](https://gitlab.com/-/ide/project/incoresemi/docker-images/) and will continue to create the Docker image that feeds the [core](https://gitlab.com/virgohardware/core) repository.  

